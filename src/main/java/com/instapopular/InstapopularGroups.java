package com.instapopular;

import com.instapopular.controller.GroupsController;
import com.instapopular.controller.impl.GroupsControllerImpl;

public class InstapopularGroups {

    public static void main(String[] args) {
        GroupsController groupsController = new GroupsControllerImpl();
        groupsController.run();
    }
}
