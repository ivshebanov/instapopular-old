package com.instapopular.constant;

public interface UnsubscribeDaoConstant {

    interface MessageConstants {
        String UNSUBSCRIBE_FROM_USERS = "unsubscribeFromUsers()";
        String GET_ALL_SUBSCRIBERS = "getAllSubscribers()";
        String GET_ALL_SUBSCRIPTIONS = "getAllSubscriptions()";
        String UNSUBSCRIBED_FROM = "unsubscribed from %d";
    }

    interface Xpath {
        String HREF = "href";
        String ACCOUNT_NAME = "//*[@id=\"react-root\"]//div/header/section/div[1]/h1";
        String OPEN_SUBSCRIPTIONS = "//*[@id=\"react-root\"]//div/header/section/ul/li[3]/a";
        String OPEN_SUBSCRIBERS = "//*[@id=\"react-root\"]//div/header/section/ul/li[2]/a";
        String SUBSCRIPTIONS_BTN = "//li[%d]/div/div[2]/button";
        //html/body/div[4]/div/div/div/div[3]/button[1] - UNSUBSCRIBE_BTN mac
        //html/body/div[3]/div/div/div/div[3]/button[1] - UNSUBSCRIBE_BTN windows
        String UNSUBSCRIBE_BTN = "/html/body/div[3]/div/div/div[3]/button[1]";
        String SCROLL = "//ul/div/li[%d]";
        String COUNT_SUBSCRIBERS = "//li[2]/a/span";
        String COUNT_SUBSCRIPTIONS = "//li[3]/a/span";
        String USER_LINK_TO_SUBSCRIBERS = "//li[%d]/div/div[1]/div[2]/div[1]/a";
    }

    interface Link {
        String HOME_PAGE = "https://www.instagram.com/%s/";
    }
}
