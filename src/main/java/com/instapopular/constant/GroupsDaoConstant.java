package com.instapopular.constant;

public interface GroupsDaoConstant {

    interface MessageConstants {
        String SUBSCRIBE_TO_GROUP_MEMBERS = "subscribeToGroupMembers(%s)";
        String SUBSCRIPTIONS = "Подписки";
        String REQUEST_SENT = "Запрос отправлен";
        String SUBSCRIBE_TO_GROUP = "subscribe from %d";
    }

    interface Xpath {
        String IS_SUBSCRIBED = "//li[%d]/div/div[2]/button";
        String URL_PHOTO = "//article/div[1]/div/div[1]/div[%d]/a";
        String CHECK_PHOTO = "//article/div/div/div[1]/div";
    }

    interface Script {
        String WINDOW_OPEN = "window.open()";
    }
}
