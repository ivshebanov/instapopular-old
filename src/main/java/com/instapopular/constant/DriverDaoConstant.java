package com.instapopular.constant;

public interface DriverDaoConstant {

    interface MessageConstants {
        String GET_LOGIN_AND_PASSWORD_FROM_PROPERTIES = "getLoginAndPasswordFromProperties()";
        String GET_HESTAG_FROM_PROPERTIES = "getHestagFromProperties()";
        String GET_GROUPS = "getGroups()";
        String GET_DRIVER = "getDriver() Local time: %tT";
        String QUIT_DRIVER = "quitDriver() Local time: %tT";
        String SET_PROPERTY = "initDriver(%s, %s)";
    }

    interface Driver {
        String WEBDRIVER = "webdriver";
        String WEBDRIVER_CHROME_DRIVER = "WEBDRIVER_CHROME_DRIVER";
        String CHROME_DRIVER = "CHROME_DRIVER";
    }

    interface propertiesName {
        String HASHTAGS = "hashtags.properties";
        String ACCOUNT = "account.properties";
        String GROUPS = "groups.properties";
    }
}
