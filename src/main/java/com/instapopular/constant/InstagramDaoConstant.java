package com.instapopular.constant;

public interface InstagramDaoConstant {

    interface MessageConstants {
        String LOGIN_ON_WEB_SITE = "loginOnWebSite(%s, %s)";
        String LOGIN_ON_WEB_SITE_SUCCESS = "loginOnWebSite() = success";
        String I_DO_NOT_LIKE = "Не нравится";
    }

    interface Xpath {
        String LOGIN_PAGE = "https://www.instagram.com/accounts/login/";
        String LOGIN_USERNAME_INPUT = "//*[@id=\"react-root\"]//div/div[1]/input[@name='username']";
        String LOGIN_PASSWORD_INPUT = "//*[@id=\"react-root\"]//div/div[1]/input[@name='password']";
        String LOGIN_BUTTON = "//*[@id=\"react-root\"]//form/div[3]/button";
        String CHECK_LOGIN_BY_NAME = "//*[@id=\"react-root\"]//div[1]/div/div[2]/div[1]/a";
        String IS_ACTIVE_LIKE = "//div[2]/section[1]/span[1]/button/span";
        String ARIA_LABEL = "aria-label";
        String SET_LIKE = "//div[2]/section[1]/span[1]/button";
    }
}
