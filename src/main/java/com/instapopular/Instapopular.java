package com.instapopular;

import com.instapopular.controller.InstagramController;
import com.instapopular.controller.impl.InstagramControllerImpl;

public class Instapopular {

    public static void main(String[] args) {
        InstagramController instagramController = new InstagramControllerImpl();
        instagramController.run();
    }
}
