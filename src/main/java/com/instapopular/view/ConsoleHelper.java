package com.instapopular.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleHelper implements View {

    private static BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

    public void writeMessage(String message) {
        System.out.println(message);
    }

    public String readString(){
        String readLine = "";
        try {
            readLine = bis.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return readLine;
    }

    @Override
    public void update() {

    }
}
