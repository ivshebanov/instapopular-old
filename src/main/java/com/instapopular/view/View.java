package com.instapopular.view;

public interface View {

    void update();

    void writeMessage(String message);
}
