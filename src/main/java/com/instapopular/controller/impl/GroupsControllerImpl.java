package com.instapopular.controller.impl;

import com.instapopular.controller.GroupsController;
import com.instapopular.dao.DriverDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.impl.DriverDaoImpl;
import com.instapopular.dao.impl.InstagramDaoImpl;
import com.instapopular.service.GroupsService;
import com.instapopular.service.impl.GroupsServiceImpl;
import java.io.IOException;
import java.util.Map;

public class GroupsControllerImpl implements GroupsController {

    @Override
    public void run() {
        DriverDao driverDao = new DriverDaoImpl();
        try {
            for (Map.Entry<String, String> entry : driverDao.getLoginAndPasswordFromProperties().entrySet()) {
                InstagramDao instagramDao = new InstagramDaoImpl(driverDao, entry.getKey(), entry.getValue());
                GroupsService groupsService = new GroupsServiceImpl(instagramDao);
                groupsService.run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            driverDao.quitDriver();
        }
    }
}
