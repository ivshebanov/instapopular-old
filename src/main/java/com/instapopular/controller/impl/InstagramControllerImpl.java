package com.instapopular.controller.impl;

import com.instapopular.controller.InstagramController;
import com.instapopular.dao.DriverDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.impl.DriverDaoImpl;
import com.instapopular.dao.impl.InstagramDaoImpl;
import com.instapopular.service.InstagramService;
import com.instapopular.service.impl.InstagramServiceImpl;
import java.io.IOException;
import java.util.Map;

public class InstagramControllerImpl implements InstagramController {

    @Override
    public void run() {
        DriverDao driverDao = new DriverDaoImpl();
        try {
            for (Map.Entry<String, String> entry : driverDao.getLoginAndPasswordFromProperties().entrySet()) {
                InstagramDao instagramDao = new InstagramDaoImpl(driverDao, entry.getKey(), entry.getValue());
                InstagramService instagramService = new InstagramServiceImpl(instagramDao);
                instagramService.run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            driverDao.quitDriver();
        }
    }
}
