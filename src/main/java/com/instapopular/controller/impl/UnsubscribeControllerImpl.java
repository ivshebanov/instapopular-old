package com.instapopular.controller.impl;

import com.instapopular.controller.UnsubscribeController;
import com.instapopular.dao.DriverDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.impl.DriverDaoImpl;
import com.instapopular.dao.impl.InstagramDaoImpl;
import com.instapopular.service.UnsubscribeService;
import com.instapopular.service.impl.UnsubscribeServiceImpl;
import java.io.IOException;
import java.util.Map;

public class UnsubscribeControllerImpl implements UnsubscribeController {

    @Override
    public void run() {
        DriverDao driverDao = new DriverDaoImpl();
        try {
            for (Map.Entry<String, String> entry : driverDao.getLoginAndPasswordFromProperties().entrySet()) {
                InstagramDao instagramDao = new InstagramDaoImpl(driverDao, entry.getKey(), entry.getValue());
                UnsubscribeService unsubscribeService = new UnsubscribeServiceImpl(instagramDao);
                unsubscribeService.run();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            driverDao.quitDriver();
        }
    }
}
