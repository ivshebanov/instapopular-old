package com.instapopular.dao;

import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Data {

    private static final Logger logger = LoggerFactory.getLogger(Data.class);
    private static ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    private static ResourceBundle resInstagram = ResourceBundle.getBundle("instagram", Locale.ENGLISH);
    private static ResourceBundle resWebdriver = ResourceBundle.getBundle("webdriver", Locale.ENGLISH);

    private WebDriver driver;

    public void init() {
        logger.debug("call void init()");
        System.setProperty(resWebdriver.getString("WEBDRIVER_CHROME_DRIVER"),
                Objects.requireNonNull(classloader.getResource(resWebdriver.getString("CHROME_DRIVER"))).getPath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);
    }

    public void driverQuit() {
        logger.debug("call void quitDriver()");
        driver.quit();
    }

    public void openUrl(String url) {
        logger.debug("call void openUrl(): " + url);
        driver.get(url);
    }

    public List<WebElement> getTopPublicationsWebElementByUrlAndHashtag(String url) {
        logger.debug("call List<WebElement> getTopPublicationsWebElementByUrlHashtag(): " + url);
        if (url == null || url.length() == 0) throw new IllegalArgumentException();
        if (!driver.getCurrentUrl().equalsIgnoreCase(url)) openUrl(url);
        return driver.findElements(By.xpath(resInstagram.getString("PATH_SEARCH_TOP_PUBLICATIONS_WEB_ELEMENT")));
    }

    public List<WebElement> getNewestWebElementByUrlHashtag(String url) {
        logger.debug("call List<WebElement> getNewestWebElementByUrlHashtag(): " + url);
        if (url == null || url.length() == 0) throw new IllegalArgumentException();
        if (!driver.getCurrentUrl().equalsIgnoreCase(url)) openUrl(url);
        ((JavascriptExecutor) driver).executeScript("scroll(0, 30000);");
        return driver.findElements(By.xpath(resInstagram.getString("PATH_SEARCH_NEWEST_WEB_ELEMENT")));

    }

    public boolean setLikeAndSubscribe(String url) {
        logger.debug("call boolean setLikeAndSubscribe(): " + url);
        if (url == null || url.length() == 0) throw new IllegalArgumentException();
        openUrl(url);
        boolean isLike = setLike();
        boolean isSubscribe = setSubscribe();
        return isLike || isSubscribe;
    }

    public boolean setOnlyLike(String url) {
        logger.debug("call boolean setOnlyLike(): " + url);
        if (url == null || url.length() == 0) throw new IllegalArgumentException();
        openUrl(url);
        return setLike();
    }

    public boolean setOnlySubscribe(String url) {
        logger.debug("call boolean setOnlySubscribe(): " + url);
        if (url == null || url.length() == 0) throw new IllegalArgumentException();
        openUrl(url);
        return setSubscribe();
    }

    private boolean setLike() {
        if (isActiveLike()) return false;
        driver.findElement(By.xpath(resInstagram.getString("LIKE_BUTTON"))).click();
        return isActiveLike();
    }

    private boolean setSubscribe() {
        if (isActiveSubscribe()) return false;
        driver.findElement(By.xpath(resInstagram.getString("SUBSCRIBE_BUTTON"))).click();
        return isActiveSubscribe();
    }

    private boolean isActiveSubscribe() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement dynamicElement = (new WebDriverWait(driver, 20))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(resInstagram.getString("SUBSCRIBE_BUTTON"))));
        String subscribe = dynamicElement.getText();
        return subscribe.equalsIgnoreCase("Подписки");
    }

    private boolean isActiveLike() {
        WebElement dynamicElement = (new WebDriverWait(driver, 20))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(resInstagram.getString("LIKE_BUTTON") + "/span")));
        String ariaLable = dynamicElement.getAttribute("aria-label");
        return ariaLable.equalsIgnoreCase("Не нравится");
    }
}
