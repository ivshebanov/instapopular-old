package com.instapopular.dao;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.WebDriver;

public interface DriverDao {

    Map<String, String> getLoginAndPasswordFromProperties() throws IOException;

    Set<String> getHestagFromProperties() throws IOException;

    Set<String> getGroups() throws IOException;

    WebDriver getDriver();

    void quitDriver();
}
