package com.instapopular.dao;

import com.instapopular.model.Photo;
import java.io.IOException;
import java.util.List;

public interface PhotoDao {

    List<Photo> getTopPublicationsWebElementByUrlHashtag(String url);

    List<Photo> getNewestWebElementByUrlHashtag(String url);

    void setLikeAndSubscribe(Photo photo);

    void setOnlyLike(Photo photo);

    void setOnlySubscribe(Photo photo);

    void updateFileData(List<Photo> photos) throws IOException;

    void addPhotosFromFile(List<Photo> photos) throws IOException;

    List<Photo> getPhotosFromFile() throws IOException;
}
