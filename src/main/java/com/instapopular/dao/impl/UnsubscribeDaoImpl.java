package com.instapopular.dao.impl;

import static com.instapopular.constant.UnsubscribeDaoConstant.MessageConstants.GET_ALL_SUBSCRIBERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.MessageConstants.GET_ALL_SUBSCRIPTIONS;
import static com.instapopular.constant.UnsubscribeDaoConstant.MessageConstants.UNSUBSCRIBED_FROM;
import static com.instapopular.constant.UnsubscribeDaoConstant.MessageConstants.UNSUBSCRIBE_FROM_USERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.COUNT_SUBSCRIBERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.COUNT_SUBSCRIPTIONS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.HREF;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.OPEN_SUBSCRIBERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.OPEN_SUBSCRIPTIONS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.SCROLL;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.SUBSCRIPTIONS_BTN;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.UNSUBSCRIBE_BTN;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.USER_LINK_TO_SUBSCRIBERS;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.UnsubscribeDao;
import com.instapopular.model.User;
import static java.lang.String.format;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnsubscribeDaoImpl implements UnsubscribeDao {

    private static final Logger logger = LoggerFactory.getLogger(UnsubscribeDaoImpl.class);
    private InstagramDao instagramDao;
    private WebDriver driver;

    public UnsubscribeDaoImpl(InstagramDao instagramDao, WebDriver driver) {
        this.instagramDao = instagramDao;
        this.driver = driver;
    }

    @Override
    public List<User> getAllSubscribers() {
        logger.info(GET_ALL_SUBSCRIBERS);
        List<User> resultUser = new ArrayList<>();
        if (instagramDao.openHomePage()) {
            String countSubscribersStr = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(COUNT_SUBSCRIBERS))).getText();
            int countSubscribers = instagramDao.convertStringToInt(countSubscribersStr);
            (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(OPEN_SUBSCRIBERS))).click();
            if (countSubscribers > 2400) {
                resultUser.addAll(getUserLinks(2400));
            }
            if (countSubscribers < 2400) {
                resultUser.addAll(getUserLinks(countSubscribers));
            }
        }
        return resultUser;
    }

    @Override
    public List<User> getAllSubscriptions() {
        logger.info(GET_ALL_SUBSCRIPTIONS);
        List<User> resultUser = new ArrayList<>();
        if (instagramDao.openHomePage()) {
            String countSubscriptionsStr = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(COUNT_SUBSCRIPTIONS))).getText();
            int countSubscriptions = instagramDao.convertStringToInt(countSubscriptionsStr);
            (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(OPEN_SUBSCRIPTIONS))).click();
            if (countSubscriptions > 2400) {
                resultUser.addAll(getUserLinks(2400));
            }
            if (countSubscriptions < 2400) {
                resultUser.addAll(getUserLinks(countSubscriptions));
            }
        }
        return resultUser;
    }

    @Override
    public void unsubscribeFromUsers(int countSubscribers, List<User> subscribers) {
        logger.info(UNSUBSCRIBE_FROM_USERS);
        if (instagramDao.openHomePage()) {
            (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(OPEN_SUBSCRIPTIONS))).click();
            instagramDao.scrollSubscriptions(20);
            for (int i = 1; i < countSubscribers; i++) {
                try {
                    instagramDao.scrollElementSubscriptions(format(SCROLL, i));
                    if (subscribers != null) {
                        if (isSubscribed(subscribers, format(USER_LINK_TO_SUBSCRIBERS, i))) {
                            continue;
                        }
                    }
                    Random random = new Random();
                    int num = 40000 + random.nextInt(80000 - 40000);
                    Thread.sleep(num);
                    (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(format(SUBSCRIPTIONS_BTN, i)))).click();
                    (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(UNSUBSCRIBE_BTN))).click();
                    logger.info(format(UNSUBSCRIBED_FROM, i));
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    instagramDao.scrollSubscriptions(i);
                }
            }
        }
    }

    private boolean isSubscribed(List<User> subscribers, String xpath) {
        String url = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))).getAttribute(HREF);
        User user = instagramDao.getUserByUrl(url);
        return subscribers.contains(user);
    }

    private List<User> getUserLinks(int count) {
        List<User> resultUrls = new ArrayList<>();
        instagramDao.scrollSubscriptions(20);
        for (int i = 1; i < count; i++) {
            try {
                instagramDao.scrollElementSubscriptions(format(SCROLL, i));
                String url = (new WebDriverWait(driver, 60)).
                        until(ExpectedConditions.presenceOfElementLocated(By.xpath(format(USER_LINK_TO_SUBSCRIBERS, i)))).getAttribute(HREF);
                resultUrls.add(instagramDao.getUserByUrl(url));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                instagramDao.scrollSubscriptions(i);
            }
        }
        return resultUrls;
    }
}
