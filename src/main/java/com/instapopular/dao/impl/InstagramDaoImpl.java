package com.instapopular.dao.impl;

import static com.instapopular.constant.GroupsDaoConstant.Script.WINDOW_OPEN;
import static com.instapopular.constant.InstagramDaoConstant.MessageConstants.I_DO_NOT_LIKE;
import static com.instapopular.constant.InstagramDaoConstant.MessageConstants.LOGIN_ON_WEB_SITE;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.ARIA_LABEL;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.CHECK_LOGIN_BY_NAME;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.IS_ACTIVE_LIKE;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.LOGIN_BUTTON;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.LOGIN_PAGE;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.LOGIN_PASSWORD_INPUT;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.LOGIN_USERNAME_INPUT;
import static com.instapopular.constant.InstagramDaoConstant.Xpath.SET_LIKE;
import static com.instapopular.constant.UnsubscribeDaoConstant.Link.HOME_PAGE;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.ACCOUNT_NAME;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.SCROLL;
import com.instapopular.dao.DriverDao;
import com.instapopular.dao.GroupsDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.PhotoDao;
import com.instapopular.dao.UnsubscribeDao;
import com.instapopular.model.User;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstagramDaoImpl implements InstagramDao {

    private static final Logger logger = LoggerFactory.getLogger(InstagramDaoImpl.class);

    private WebDriver driver;
    private String login;
    private String password;
    private PhotoDao photoDao;
    private UnsubscribeDao unsubscribeDao;
    private DriverDao driverDao;
    private GroupsDao groupsDao;

    public InstagramDaoImpl(DriverDao driver, String login, String password) {
        this.driverDao = driver;
        this.driver = driver.getDriver();
        this.login = login;
        this.password = password;
        this.photoDao = new PhotoDaoImpl(this, this.driver, login);
        this.unsubscribeDao = new UnsubscribeDaoImpl(this, this.driver);
        this.groupsDao = new GroupsDaoImpl(this, this.driver);
    }

    @Override
    public boolean loginOnWebSite() {
        logger.info(format(LOGIN_ON_WEB_SITE, login, password));
        openUrl(LOGIN_PAGE);
        (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(LOGIN_USERNAME_INPUT))).sendKeys(login);
        (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(LOGIN_PASSWORD_INPUT))).sendKeys(password);
        (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(LOGIN_BUTTON))).click();
        String accountName = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(CHECK_LOGIN_BY_NAME))).getText();
        if (login.equalsIgnoreCase(accountName)) {
            return true;
        }
        return false;
    }

    @Override
    public PhotoDao getPhotoDao() {
        return photoDao;
    }

    @Override
    public DriverDao getDriverDao() {
        return driverDao;
    }

    @Override
    public UnsubscribeDao getUnsubscribeDao() {
        return unsubscribeDao;
    }

    @Override
    public GroupsDao getDroupsDao() {
        return groupsDao;
    }

    @Override
    public boolean openHomePage() {
        if (!driver.getCurrentUrl().equalsIgnoreCase(format(HOME_PAGE, login))) {
            openUrl(format(HOME_PAGE, login));
        }
        String accountName = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(ACCOUNT_NAME))).getText();
        if (login.equalsIgnoreCase(accountName)) {
            return TRUE;
        }
        return FALSE;
    }

    @Override
    public int convertStringToInt(String count) {
        if (count.length() == 1 || count.length() == 2 || count.length() == 3) {
            return Integer.parseInt(count);
        }
        if (count.length() == 5) {
            return Integer.parseInt(removeCharAt(count, 1));
        }
        if (count.length() == 6) {
            return Integer.parseInt(removeCharAt(count, 2));
        }
        if (count.length() == 7) {
            return Integer.parseInt(removeCharAt(count, 3));
        }
        return 0;
    }

    @Override
    public void scrollSubscriptions(int currentPosition) {
        for (int i = 1; i <= currentPosition; i++) {
            try {
                scrollElementSubscriptions(format(SCROLL, i));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void scrollElementSubscriptions(String xPathElement) {
        WebElement scroll = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPathElement)));
        Coordinates coordinate = ((Locatable) scroll).getCoordinates();
        coordinate.onPage();
        coordinate.inViewPort();
    }

    private static String removeCharAt(String s, int pos) {
        return s.substring(0, pos) + s.substring(pos + 1);
    }

    @Override
    public User getUserByUrl(String url) {
        User user = new User();
        user.setLinkAccount(url);
        user.setName(genLoginByUrl(url));
        return user;
    }

    @Override
    public String genLoginByUrl(String url) {
        return url.substring(26, url.length() - 1);
    }

    @Override
    public String openUrl(String url) {
        if (url != null && url.length() != 0) {
            driver.get(url);
        }
        return driver.getWindowHandle();
    }

    @Override
    public String openUrlNewTab(String url) {
        ((JavascriptExecutor) driver).executeScript(WINDOW_OPEN);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        selectTab(tabs.size() - 1);
        return openUrl(url);
    }

    @Override
    public void selectTab(int countTab) {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        selectTab(tabs.get(countTab));
    }

    @Override
    public void selectTab(String windowHandle) {
        driver.switchTo().window(windowHandle);
    }

    @Override
    public void closeTab(int countTab) {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        closeTab(tabs.get(countTab));
    }

    @Override
    public void closeTab(String windowHandle) {
        selectTab(windowHandle);
        driver.close();
    }

    @Override
    public void setLike(String urlPhoto) {
        String currentWindowHandle = driver.getWindowHandle();
        String photoWindowHandle = null;
        try {
            photoWindowHandle = openUrlNewTab(urlPhoto);
            if (!isActiveLike()) {
                (new WebDriverWait(driver, 60))
                        .until(ExpectedConditions.presenceOfElementLocated(By.xpath(SET_LIKE))).click();
            }

        } catch (Exception ignored){

        }finally {
            closeTab(photoWindowHandle);
            selectTab(currentWindowHandle);
        }
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    private boolean isActiveLike() {
        String ariaLable = (new WebDriverWait(driver, 60))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(IS_ACTIVE_LIKE)))
                .getAttribute(ARIA_LABEL);
        return ariaLable.equalsIgnoreCase(I_DO_NOT_LIKE);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getLogin() {
        return login;
    }
}
