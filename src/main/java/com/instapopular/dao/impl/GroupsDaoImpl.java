package com.instapopular.dao.impl;

import static com.instapopular.constant.GroupsDaoConstant.MessageConstants.REQUEST_SENT;
import static com.instapopular.constant.GroupsDaoConstant.MessageConstants.SUBSCRIBE_TO_GROUP;
import static com.instapopular.constant.GroupsDaoConstant.MessageConstants.SUBSCRIBE_TO_GROUP_MEMBERS;
import static com.instapopular.constant.GroupsDaoConstant.MessageConstants.SUBSCRIPTIONS;
import static com.instapopular.constant.GroupsDaoConstant.Xpath.CHECK_PHOTO;
import static com.instapopular.constant.GroupsDaoConstant.Xpath.IS_SUBSCRIBED;
import static com.instapopular.constant.GroupsDaoConstant.Xpath.URL_PHOTO;
import static com.instapopular.constant.UnsubscribeDaoConstant.Link.HOME_PAGE;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.COUNT_SUBSCRIBERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.HREF;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.OPEN_SUBSCRIBERS;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.SCROLL;
import static com.instapopular.constant.UnsubscribeDaoConstant.Xpath.USER_LINK_TO_SUBSCRIBERS;
import com.instapopular.dao.GroupsDao;
import com.instapopular.dao.InstagramDao;
import static java.lang.String.format;
import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroupsDaoImpl implements GroupsDao {

    private static final Logger logger = LoggerFactory.getLogger(GroupsDaoImpl.class);
    private InstagramDao instagramDao;
    private WebDriver driver;

    public GroupsDaoImpl(InstagramDao instagramDao, WebDriver driver) {
        this.instagramDao = instagramDao;
        this.driver = driver;
    }

    @Override
    public void subscribeToGroupMembers(String channelName) {
        logger.info(format(SUBSCRIBE_TO_GROUP_MEMBERS, channelName));
        String baseWindowHandle = null;
        if (!instagramDao.getCurrentUrl().equalsIgnoreCase(format(HOME_PAGE, channelName))) {
            baseWindowHandle = instagramDao.openUrl(format(HOME_PAGE, channelName));
        }
//        String countSubscribersStr = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(COUNT_SUBSCRIBERS))).getText();
        int countSubscribers = 300;
//        instagramDao.convertStringToInt(countSubscribersStr);
//        if (countSubscribers > 300) {
//            countSubscribers = 300;
//        }
        (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(OPEN_SUBSCRIBERS))).click();
        instagramDao.scrollSubscriptions(320);
        for (int i = 1; i < countSubscribers; i++) {
            try {
                instagramDao.scrollElementSubscriptions(format(SCROLL, i));
                if (isSubscribed(format(IS_SUBSCRIBED, i))) {
                    countSubscribers++;
                    continue;
                }
                (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(format(IS_SUBSCRIBED, i)))).click();
                Thread.sleep(2000);
                if (requestSent(format(IS_SUBSCRIBED, i))) {
                    continue;
                }

                String urlUser = (new WebDriverWait(driver, 60)).
                        until(ExpectedConditions.presenceOfElementLocated(By.xpath(format(USER_LINK_TO_SUBSCRIBERS, i)))).getAttribute(HREF);
                String userWindowHandle = instagramDao.openUrlNewTab(urlUser);
                int countPhoto;
                try {
                    countPhoto = checkPhoto();
                } catch (Exception e) {
                    instagramDao.closeTab(userWindowHandle);
                    instagramDao.selectTab(baseWindowHandle);
                    continue;
                }

                if (countPhoto != 0) {
                    for (int j = 1; j <= 3; j++) {
                        try {
                            String urlPhoto = (new WebDriverWait(driver, 60)).
                                    until(ExpectedConditions.presenceOfElementLocated(By.xpath(format(URL_PHOTO, j)))).getAttribute(HREF);
                            instagramDao.setLike(urlPhoto);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                            instagramDao.selectTab(userWindowHandle);
                        }
                    }
                }
                logger.info(format(SUBSCRIBE_TO_GROUP, i));
                instagramDao.closeTab(userWindowHandle);
                instagramDao.selectTab(baseWindowHandle);
                Random random = new Random();
                int num = 20000 + random.nextInt(100000 - 20000);
                Thread.sleep(num);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                instagramDao.selectTab(baseWindowHandle);
            }
        }
    }

    private boolean isSubscribed(String xpath) {
        String isSubscribed = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))).getText();
        return isSubscribed.equalsIgnoreCase(SUBSCRIPTIONS) || isSubscribed.equalsIgnoreCase(REQUEST_SENT);
    }

    private boolean requestSent(String xpath) {
        String isSubscribed = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath))).getText();
        return isSubscribed.equalsIgnoreCase(REQUEST_SENT);
    }

    private int checkPhoto() {
        List<WebElement> webElement = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(CHECK_PHOTO)));
        return (webElement == null) ? 0 : webElement.size();
    }
}
