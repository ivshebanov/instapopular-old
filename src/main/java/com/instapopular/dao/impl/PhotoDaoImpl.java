package com.instapopular.dao.impl;

import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.PhotoDao;
import com.instapopular.model.Bool;
import static com.instapopular.model.Bool.FALSE;
import static com.instapopular.model.Bool.TRUE;
import com.instapopular.model.Photo;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.ResourceBundle;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhotoDaoImpl implements PhotoDao {

    private static final Logger logger = LoggerFactory.getLogger(PhotoDaoImpl.class);
    private final String classPath = PhotoDaoImpl.class.getPackage().getName();
    private final String filePath = "./src/main/java/" + classPath.substring(0, classPath.lastIndexOf(".")).replaceAll("\\.", "/") + "/data/dataPhoto/photo.html";
    //    private final String filePath = "/Users/iliashebanov/Documents/Java/instapopular/src/main/java/com/instapopular/dao/data/dataPhoto/photo.html";
    private static ResourceBundle resInstagram = ResourceBundle.getBundle("instagram", Locale.ENGLISH);

    private InstagramDao instagramDao;
    private WebDriver driver;
    private String login;

    public PhotoDaoImpl(InstagramDao instagramDao, WebDriver driver, String login) {
        this.instagramDao = instagramDao;
        this.driver = driver;
        this.login = login;
    }

    @Override
    public List<Photo> getTopPublicationsWebElementByUrlHashtag(String url) {
        logger.info("call List<Photo> getTopPublicationsWebElementByUrlHashtag(): " + url);
        if (url == null || url.length() == 0) return null;

        if (!driver.getCurrentUrl().equalsIgnoreCase(url)) instagramDao.openUrl(url);
        List<WebElement> we = driver.findElements(By.xpath(resInstagram.getString("PATH_SEARCH_TOP_PUBLICATIONS_WEB_ELEMENT")));
        logger.debug("getTopPublicationsWebElementByUrlHashtag() we.size() = " + we.size());
        return getListPhotoByWebElement(we);

    }

    @Override
    public List<Photo> getNewestWebElementByUrlHashtag(String url) {
        logger.info("call List<Photo> getNewestWebElementByUrlHashtag(): " + url);
        if (url == null || url.length() == 0) return null;

        if (!driver.getCurrentUrl().equalsIgnoreCase(url)) instagramDao.openUrl(url);
        ((JavascriptExecutor) driver).executeScript("scroll(0, 40000);");
        List<WebElement> we = driver.findElements(By.xpath(resInstagram.getString("PATH_SEARCH_NEWEST_WEB_ELEMENT")));
        logger.debug("getNewestWebElementByUrlHashtag() we.size() = " + we.size());
        return getListPhotoByWebElement(we);

    }

    @Override
    public void setLikeAndSubscribe(Photo photo) {
        logger.info("call void setLikeAndSubscribe(): " + photo);
        if (photo != null) {
            instagramDao.openUrl(photo.getLinkPhoto());
            photo.setIsActiveLike(setLike());
            logger.debug("setLikeAndSubscribe(): isActiveLike = " + photo.isActiveLike());
            photo.setIsActiveSubscribed(setSubscribe());
            logger.debug("setLikeAndSubscribe(): isActiveSubscribed = " + photo.isActiveSubscribed());
        }
    }

    @Override
    public void setOnlyLike(Photo photo) {
        logger.info("call void setOnlyLike(): " + photo);
        if (photo != null) {
            instagramDao.openUrl(photo.getLinkPhoto());
            photo.setIsActiveLike(setLike());
            logger.debug("setOnlyLike(): isActiveLike = " + photo.isActiveLike());
        }
    }

    @Override
    public void setOnlySubscribe(Photo photo) {
        logger.info("call void setOnlySubscribe(): " + photo);
        if (photo != null) {
            instagramDao.openUrl(photo.getLinkPhoto());
            photo.setIsActiveSubscribed(setSubscribe());
            logger.debug("setOnlySubscribe(): isActiveSubscribed = " + photo.isActiveSubscribed());
        }
    }

    @Override
    public void updateFileData(List<Photo> photos) throws IOException {
        logger.info("call void updateFileData(): " + photos);
        updateFile(updatedFileContent(photos));
    }

    @Override
    public void addPhotosFromFile(List<Photo> photos) throws IOException {
        List<Photo> oldPhotos = getPhotosFromFile();
        oldPhotos.addAll(photos);
        updateFileData(oldPhotos);
    }

    @Override
    public List<Photo> getPhotosFromFile() throws IOException {
        logger.info("call List<Photo> getPhotosFromFile()");
        List<Photo> resultList = new LinkedList<>();
        Document doc;
        doc = requireNonNull(getDocument());
        Elements elements = requireNonNull(doc.getElementsByClass("photo"));
        if (elements.isEmpty()) return resultList;
        for (Element element : elements) {
            if (element == null) break;
            Element name = element.getElementsByClass("name").first();
            if (name.text().isEmpty()) break;
            Element linkAccount = element.getElementsByClass("linkAccount").first();
            Element linkPhoto = element.getElementsByClass("linkPhoto").first();
            Element isActiveSubscribed = element.getElementsByClass("isActiveSubscribed").first();
            Element isActiveLike = element.getElementsByClass("isActiveLike").first();

            Photo photo = new Photo();
            photo.setName(name.text());
            photo.setLinkAccount(linkAccount.getElementsByTag("a").attr("href"));
            photo.setLinkPhoto(linkPhoto.getElementsByTag("a").attr("href"));
            String isActiveSubscribedStr = isActiveSubscribed.text();
            Bool isActiveSubscribedBool = isActiveSubscribedStr == null || isActiveSubscribedStr.isEmpty() ? null : Bool.valueOf(isActiveSubscribedStr);
            photo.setIsActiveSubscribed(isActiveSubscribedBool);
            String isActiveLikeStr = isActiveLike.text();
            Bool isActiveLikeBool = isActiveLikeStr == null || isActiveLikeStr.isEmpty() ? null : Bool.valueOf(isActiveLikeStr);
            photo.setIsActiveLike(isActiveLikeBool);
            resultList.add(photo);
        }
        return resultList;
    }

    private List<Photo> getListPhotoByWebElement(List<WebElement> elements) {
        if (elements == null || elements.size() == 0) return null;
        List<String> urls = new LinkedList<>();
        for (WebElement webElement : elements) {
            urls.add(getUrlsPhotoByElement(webElement));
        }
        List<Photo> resultList = new LinkedList<>();
        for (String url : urls) {
            Photo photo = getPhotoByWebElement(url);
            if (photo != null) {
                resultList.add(photo);
            }
        }
        return resultList;
    }

    private Photo getPhotoByWebElement(String urlPhoto) {
        if (urlPhoto == null || urlPhoto.length() == 0) return null;
        if (!driver.getCurrentUrl().equalsIgnoreCase(urlPhoto)) instagramDao.openUrl(urlPhoto);
        Photo resultPhoto = new Photo();
        WebElement dynamicElement = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(resInstagram.getString("PHOTO_NAME"))));
        resultPhoto.setName(dynamicElement.getText());
        resultPhoto.setLinkAccount(dynamicElement.getAttribute("href"));
        resultPhoto.setLinkPhoto(driver.getCurrentUrl());
        resultPhoto.setIsActiveLike(isActiveLike());
        resultPhoto.setIsActiveSubscribed(isActiveSubscribe());
        return resultPhoto;
    }

    private String getUrlsPhotoByElement(WebElement element) {
        if (element == null) return null;
        return element.getAttribute("href");
    }

    private String updatedFileContent(List<Photo> photos) throws IOException {
        Document document = getDocument();

        Element templateOriginal = document.getElementsByClass("template").first();
        Element copyTemplate = templateOriginal.clone();
        copyTemplate.removeAttr("style");
        copyTemplate.removeClass("template");
        document.select("tr[class=photo]").remove();

        for (Photo photo : photos) {
            Element localClone = copyTemplate.clone();
            localClone.getElementsByClass("name").first().text(photo.getName());

            Element linkAccount = localClone.getElementsByTag("a").first();
            linkAccount.text(photo.getName());
            linkAccount.attr("href", photo.getLinkAccount());

            Element linkPhoto = localClone.getElementsByTag("a").last();
            linkPhoto.text(photo.getLinkPhoto().substring(photo.getLinkPhoto().lastIndexOf("/p/")));
            linkPhoto.attr("href", photo.getLinkPhoto());

            localClone.getElementsByClass("isActiveSubscribed").first().text(photo.isActiveSubscribed().toString());
            localClone.getElementsByClass("isActiveLike").first().text(photo.isActiveLike().toString());
            templateOriginal.before(localClone.outerHtml());
        }
        return document.html();
    }

    private Document getDocument() throws IOException {
        return Jsoup.parse(new File(filePath), "UTF-8");
    }

    private void updateFile(String photo) {
        try (FileWriter fileWriter = new FileWriter(new File(filePath))) {
            fileWriter.write(photo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Bool setLike() {
        if (Objects.equals(isActiveLike(), TRUE)) return TRUE;
        driver.findElement(By.xpath(resInstagram.getString("LIKE_BUTTON"))).click();
        if (Objects.equals(isActiveLike(), TRUE)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private Bool setSubscribe() {
        if (Objects.equals(isActiveSubscribe(), TRUE)) return TRUE;
        driver.findElement(By.xpath(resInstagram.getString("SUBSCRIBE_BUTTON"))).click();
        if (Objects.equals(isActiveSubscribe(), TRUE)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private Bool isActiveSubscribe() {
        try {
            Thread.sleep(1500);
            WebElement dynamicElement = (new WebDriverWait(driver, 20))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath(resInstagram.getString("SUBSCRIBE_BUTTON"))));
            String subscribe = dynamicElement.getText();
            if (subscribe == null || subscribe.length() == 0) return null;
            if (subscribe.equalsIgnoreCase("Подписки")) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception e) {
            logger.error("isActiveLike " + e.getMessage());
            e.printStackTrace();
            return TRUE;
        }
    }

    private Bool isActiveLike() {
        try {
            WebElement dynamicElement = (new WebDriverWait(driver, 20))
                    .ignoring(NoSuchElementException.class)
                    .ignoring(StaleElementReferenceException.class)
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath(resInstagram.getString("LIKE_BUTTON") + "/span")));
            String ariaLable = dynamicElement.getAttribute("aria-label");
            if (ariaLable == null || ariaLable.length() == 0) return null;
            if (ariaLable.equalsIgnoreCase("Не нравится")) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (Exception e) {
            logger.error("isActiveLike " + e.getMessage());
            e.printStackTrace();
            return TRUE;
        }
    }
}