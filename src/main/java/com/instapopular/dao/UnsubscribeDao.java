package com.instapopular.dao;

import com.instapopular.model.User;
import java.util.List;

public interface UnsubscribeDao {

    List<User> getAllSubscribers();

    List<User> getAllSubscriptions();

    void unsubscribeFromUsers(int countSubscribers, List<User> subscribers);
}
