package com.instapopular.dao;

public interface GroupsDao {

    void subscribeToGroupMembers(String channelName);
}
