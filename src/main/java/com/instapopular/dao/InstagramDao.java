package com.instapopular.dao;

import com.instapopular.model.User;

public interface InstagramDao {

    boolean loginOnWebSite();

    PhotoDao getPhotoDao();

    DriverDao getDriverDao();

    UnsubscribeDao getUnsubscribeDao();

    GroupsDao getDroupsDao();

    boolean openHomePage();

    int convertStringToInt(String count);

    void scrollSubscriptions(int currentPosition);

    void scrollElementSubscriptions(String xPathElement);

    User getUserByUrl(String url);

    String genLoginByUrl(String url);

    String openUrl(String url);

    String openUrlNewTab(String url);

    void selectTab(int countTab);

    void selectTab(String windowHandle);

    void closeTab(int countTab);

    void closeTab(String windowHandle);

    void setLike(String urlPhoto);

    String getCurrentUrl();
}
