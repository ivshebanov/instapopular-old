package com.instapopular;

import com.instapopular.controller.UnsubscribeController;
import com.instapopular.controller.impl.UnsubscribeControllerImpl;

public class InstapopularUnsubscribe {

    public static void main(String[] args) {
        UnsubscribeController unsubscribeController = new UnsubscribeControllerImpl();
        unsubscribeController.run();
    }
}
