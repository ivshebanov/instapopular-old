package com.instapopular.model;

public class Photo {
    private String name;                //имя акаунта
    private String linkAccount;         //сслыка на акаунт
    private String linkPhoto;           //ссылка на фото
    private Bool isActiveSubscribed;       //мы подписаны?
    private Bool isActiveLike;             //нам нравится?

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkAccount() {
        return linkAccount;
    }

    public void setLinkAccount(String linkAccount) {
        this.linkAccount = linkAccount;
    }

    public String getLinkPhoto() {
        return linkPhoto;
    }

    public void setLinkPhoto(String linkPhoto) {
        this.linkPhoto = linkPhoto;
    }

    public Bool isActiveSubscribed() {
        return isActiveSubscribed;
    }

    public void setIsActiveSubscribed(Bool isActiveSubscribed) {
        this.isActiveSubscribed = isActiveSubscribed;
    }

    public Bool isActiveLike() {
        return isActiveLike;
    }

    public void setIsActiveLike(Bool isActiveLike) {
        this.isActiveLike = isActiveLike;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "name='" + name + '\'' +
                ", linkAccount='" + linkAccount + '\'' +
                ", linkPhoto='" + linkPhoto + '\'' +
                ", isActiveSubscribed=" + isActiveSubscribed +
                ", isActiveLike=" + isActiveLike +
                '}';
    }
}
