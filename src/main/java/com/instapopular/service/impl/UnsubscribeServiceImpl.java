package com.instapopular.service.impl;

import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.UnsubscribeDao;
import com.instapopular.service.UnsubscribeService;

public class UnsubscribeServiceImpl implements UnsubscribeService {

    private InstagramDao instagramDao;
    private UnsubscribeDao unsubscribeDao;

    public UnsubscribeServiceImpl(InstagramDao instagramDao) {
        this.instagramDao = instagramDao;
        this.unsubscribeDao = instagramDao.getUnsubscribeDao();
    }

    @Override
    public void run() {
        instagramDao.loginOnWebSite();
//        List<User> subscribers = unsubscribeDao.getAllSubscribers();
//        List<User> subscriptions = unsubscribeDao.getAllSubscriptions();
        unsubscribeDao.unsubscribeFromUsers(400, null);
    }
}
