package com.instapopular.service.impl;

import com.instapopular.dao.DriverDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.dao.PhotoDao;
import static com.instapopular.model.Bool.FALSE;
import com.instapopular.model.Photo;
import com.instapopular.service.InstagramService;
import java.io.IOException;
import static java.lang.String.format;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InstagramServiceImpl implements InstagramService {
    private static final Logger logger = LoggerFactory.getLogger(InstagramService.class);
    private static ResourceBundle resWebdriver = ResourceBundle.getBundle("instagram", Locale.ENGLISH);
    private InstagramDao instagramDao;
    private PhotoDao photoDao;

    public InstagramServiceImpl(InstagramDao instagramDao) {
        this.instagramDao = instagramDao;
        this.photoDao = instagramDao.getPhotoDao();
    }

    @Override
    public void run() {
        try {
            Set<String> hestags = instagramDao.getDriverDao().getHestagFromProperties();
            instagramDao.loginOnWebSite();
            for (String hestag : hestags) {
                String urlByHeshteg = format(resWebdriver.getString("URL_TO_OPEN_THE_HASHTAG"), hestag);
//                List<Photo> photos = photoDao.getTopPublicationsWebElementByUrlHashtag(urlByHeshteg);
                List<Photo> photos = photoDao.getNewestWebElementByUrlHashtag(urlByHeshteg);
//                photos.addAll(photoDao.getNewestWebElementByUrlHashtag(urlByHeshteg));

                if (photos != null && photos.size() != 0) {
                    for (Photo photo : photos) {
                        if (photo.isActiveLike() == FALSE || photo.isActiveSubscribed() == FALSE) {
                            Random random = new Random();
                            int num = 40000 + random.nextInt(80000 - 40000);
                            Thread.sleep(num);
                            photoDao.setOnlyLike(photo);
                        }
                    }
                }
            }
//            photoDao.addPhotosFromFile(photos);
//            List<Photo> re = photoDao.getPhotosFromFile();
//            System.out.println(re);
        } catch (IOException | InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean loginOnWebSite() {
        return instagramDao.loginOnWebSite();
    }

    @Override
    public void openUrl(String url) throws TimeoutException {
        instagramDao.openUrl(url);
    }

    @Override
    public PhotoDao getPhotoDao() {
        return instagramDao.getPhotoDao();
    }

    @Override
    public DriverDao getDriverDao() {
        return instagramDao.getDriverDao();
    }
}
