package com.instapopular.service.impl;

import com.instapopular.dao.GroupsDao;
import com.instapopular.dao.InstagramDao;
import com.instapopular.service.GroupsService;
import java.io.IOException;
import java.util.Set;

public class GroupsServiceImpl implements GroupsService {

    private InstagramDao instagramDao;
    private GroupsDao groupsDao;

    public GroupsServiceImpl(InstagramDao instagramDao) {
        this.instagramDao = instagramDao;
        this.groupsDao = instagramDao.getDroupsDao();
    }

    @Override
    public void run() {
        try {
            instagramDao.loginOnWebSite();
            Set<String> groups = instagramDao.getDriverDao().getGroups();
            for (String urlGroup : groups){
                groupsDao.subscribeToGroupMembers(urlGroup);
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
            instagramDao.getDriverDao().quitDriver();
        }
    }
}
