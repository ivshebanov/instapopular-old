package com.instapopular.service;

import com.instapopular.dao.DriverDao;
import com.instapopular.dao.PhotoDao;
import com.instapopular.model.Bool;
import java.util.concurrent.TimeoutException;

public interface InstagramService {

    void run();

    boolean loginOnWebSite();

    void openUrl(String url) throws TimeoutException;

    PhotoDao getPhotoDao();

    DriverDao getDriverDao();
}
